import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Main {

    // Complete the kangaroo function below.
    static String kangaroo(int x1, int v1, int x2, int v2) {

        while(x2>=x1 && v2<=v1) {
            if (x1 == x2) {
                return "YES";
            } else{
                x1+=v1;
                x2+=v2;
            }

        }
        return "NO";
    }


    public static void main(String[] args) throws IOException {

        //int x1 = 0, v1 = 3;
        //int x2 = 4, v2 = 2;

        int x1 = 0, v1 = 2;
        int x2 = 5, v2 = 3;

        String result = kangaroo(x1, v1, x2, v2);
        System.out.println(result);
    }
}
